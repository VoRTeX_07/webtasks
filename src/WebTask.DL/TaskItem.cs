﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebTask.DL
{
    public class TaskItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
