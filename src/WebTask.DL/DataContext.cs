﻿using Microsoft.EntityFrameworkCore;

namespace WebTask.DL
{
   public class DataContext : DbContext
    {    
        public DataContext(DbContextOptions<DataContext> options)
            :base(options)
        { 
        }
        public DbSet<TaskItem> Tasks { get; set; }
    }
}
