﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using src.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using WebTask.BL;
using WebTask.DL;
using WebTask.Helper;
using WebTask.ViewModel;

namespace WebTask.Controllers
{
    public class TaskActionController: Controller
    {
        private readonly ITaskService _taskService;
        private readonly IMapper _mapper;
        
        public TaskActionController(ITaskService taskService, IMapper mapper)
        {
            _taskService = taskService;
            _mapper = mapper;
        }
        [HttpGet]
        
        public IActionResult Index([FromQuery] int page, [FromQuery] int size)
        {
           var result = PagingHelper.ChekPagingOptions(page,size);
            var allList = _taskService.GetAll(result.Page,result.Size);
            var listView = _mapper.Map<List<TaskViewModel>>(allList);
            return View(listView);
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Create(TaskItemCreatModel taskItemCreatModel)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    var item = _mapper.Map<TaskItem>(taskItemCreatModel);
                   _taskService.Insert(item);
                }
                else
                {
                    return ViewBag["Error"] = "Ошибка";
                }
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return ViewBag["Error"] = e.Message;
            }
        }
        
        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            if (id == Guid.Empty) 
            {
                return ViewBag["Error"] = "Задача пустая";
            }
            
            var modelDelete = _taskService.Get(id);
            if (modelDelete == null)
            {
                return ViewBag["Error"] = "Задача отсутвует";
            }
            var taskViewModel = _mapper.Map<TaskItemDeleteModel>(modelDelete);
            return View(taskViewModel);
        }

        [HttpPost]  
        public IActionResult Delete(TaskItemDeleteModel taskItemDeleteModel)
        {
            try
            {
                var item = _mapper.Map<TaskItem>(taskItemDeleteModel);
                var deleteItem =  _taskService.Get(item.Id);
                if (deleteItem == null)
                {
                    return ViewBag["Error"] = "Отсутвует задача";
                }
                _taskService.Delete(deleteItem);
               return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return ViewBag["Error"] = e.Message;
            }
        }
        
        [HttpGet]
        public  IActionResult Details(Guid id)
        {
            if (id == Guid.Empty)
            {
                return ViewBag["Error"] = "Задача пустая";
            }
            var modelDetails = _taskService.Get(id);
            if (modelDetails == null)
            {
                return ViewBag["Error"] = "Задача отсутствует";
            }
            var taskViewModel = _mapper.Map<TaskItemDetailsModel>(modelDetails);
                return View(taskViewModel);
            
        }
        [HttpGet]
        
        public IActionResult Edit(Guid id)
        {
            if (id==Guid.Empty)
            {
                return ViewBag["Error"] = "Задача пустая";
            }
            var modelEdit =  _taskService.Get(id);
            if (modelEdit == null)
            {
                return ViewBag["Error"] = "Задача отсутвует";
            } 
            var taskViewModel = _mapper.Map<TaskItemEditModel>(modelEdit);
            return View(taskViewModel);
            
        }
        
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Edit(TaskItemEditModel taskItemEditModel)
        {
            try
            {
                if (!ModelState.IsValid) 
                   return ViewBag["Error"] = "Ошибка валидации";
               
               var item = _taskService.Get(taskItemEditModel.Id);
               if (item == null)
                   return ViewBag["Error"] = "Сущность не найдена";
               
               item = _mapper.Map<TaskItemEditModel,TaskItem>(taskItemEditModel, item);

               _taskService.Update(item);
               
               return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return ViewBag["Error"] = e.Message;
            }
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
