﻿using System;

namespace WebTask.Helper
{
    public  class PagingHelper
    {
        public static (int Page, int Size) ChekPagingOptions(int page, int size)
        {
            if (page <= 0)
            {
                page = 1;
            }

            if (size <= 0)
            {
                size = 3;
            }
            
            return (page,size);
        }
    }
    
}
