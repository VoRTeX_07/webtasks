﻿using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebTask.Helper
{
    public class MessageBox
    {
        public static void Show(Page Page, string Message) {
            Page.ClientScript.RegisterStartupScript(
                Page.GetType(),
                "MessageBox",
                "<script language='javascript'>alert('" + Message + "');</script>"
            );
        }
    }
}