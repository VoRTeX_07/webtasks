﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebTask.ViewModel
{
    public class TaskItemDetailsModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
    }
}