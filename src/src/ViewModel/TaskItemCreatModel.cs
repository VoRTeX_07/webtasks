﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebTask.ViewModel
{
    public class TaskItemCreatModel
    {
        [Required]
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
    }
}