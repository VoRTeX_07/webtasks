﻿using System;
using System.ComponentModel.DataAnnotations;


namespace WebTask.DL.ViewModel
{
    public class TaskItemCreateModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
    }
}