﻿using AutoMapper;

namespace WebTask.DL.ViewModel
{
    public class TaskItemDetailsModelProfile : Profile

    {
        public TaskItemDetailsModelProfile()
        {
            CreateMap<TaskItem, TaskItemDetailsModel>()
                .ReverseMap();
        }
    }
}