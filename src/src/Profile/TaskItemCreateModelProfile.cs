﻿using System.Threading;
using AutoMapper;

namespace WebTask.DL.ViewModel
{
    public class TaskItemCreateModelProfile:Profile
    {
        public TaskItemCreateModelProfile()
        {
            CreateMap<TaskItem, TaskItemCreateModel>()
                .ReverseMap();
        }
    }
}