﻿using WebTask.DL;
using WebTask.ViewModel;

namespace WebTask.Profile
{
    public class TaskViewModelProfile: AutoMapper.Profile
    {
        public TaskViewModelProfile()
        {
            CreateMap<TaskItem, TaskViewModel>()
           .ReverseMap();
            
            CreateMap<TaskItem, TaskItemEditModel>()
                .ReverseMap();
            
            CreateMap<TaskItem, TaskItemDetailsModel>()
                .ReverseMap();
            
            CreateMap<TaskItem, TaskItemDeleteModel>()
                .ReverseMap();
            
            CreateMap<TaskItem, TaskItemCreatModel>()
                .ReverseMap();
        }
    }
}
