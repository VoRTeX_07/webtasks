﻿using AutoMapper;

namespace WebTask.DL.ViewModel
{
    public class TaskItemEditModelProfile:Profile
    {
        public TaskItemEditModelProfile()
        {
            CreateMap<TaskItem, TaskItemEditModel>()
                .ReverseMap();
        }
    }
}