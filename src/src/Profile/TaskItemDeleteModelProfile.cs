﻿using AutoMapper;
using AutoMapper.Mappers;

namespace WebTask.DL.ViewModel
{
    public class TaskItemDeleteModelProfile:Profile
    {
        public TaskItemDeleteModelProfile()
        {
            CreateMap<TaskItem, TaskItemDeleteModel>()
                .ReverseMap();
        }
    }
}