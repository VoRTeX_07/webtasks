﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebTask.DL;

namespace WebTask.BL
{
    public interface ITaskService
    {
       TaskItem Insert(TaskItem task);
        void Update(TaskItem task);
        void Delete(TaskItem task);
        IEnumerable <TaskItem> GetAll(int page, int size);
        TaskItem Get(Guid id);
    }
}
