﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebTask.DL;

namespace WebTask.BL
{
    public class TaskService : ITaskService
    {
        private readonly DataContext _dataContext;

        public TaskService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public TaskItem Insert(TaskItem task)
        {
           
          var newTask =  _dataContext.Tasks.Add(task);
            _dataContext.SaveChanges();
            return newTask.Entity;
        }
        public void Update(TaskItem task)
        {
            
            _dataContext.Tasks.Update(task);
            _dataContext.SaveChanges();
        }
        public void Delete(TaskItem task)
        {
            _dataContext.Tasks.Remove(task);
            _dataContext.SaveChanges();
        }
        public IEnumerable<TaskItem> GetAll(int page, int size)
        { 
            var make = _dataContext.Tasks.OrderBy(t => t.CreateDate).ToList();
            var items = make.Skip((page - 1) * size).Take(size).ToList();
            return items;
        }
        public TaskItem Get(Guid id)
        {
            return  _dataContext.Tasks.FirstOrDefault(x => x.Id == id);
        }
        
    }
}
